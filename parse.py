import csv
from geopy.geocoders import GeoNames
from rdflib import URIRef, BNode, Literal, Namespace, Graph
from rdflib.namespace import NamespaceManager, RDF
import requests
import urllib

geolocator = GeoNames(country_bias="United Kingdom", username="craigsnowden")
g = Graph()
nsMgr = NamespaceManager(g)
os_namespace = Namespace("http://data.ordnancesurvey.co.uk/id/")
dbpedia_namespace = Namespace("http://dbpedia.org/property/")
dbpedia_resources = Namespace("http://dbpedia.org/resource/")
reldem_namespace = Namespace("http://vocab.inf.ed.ac.uk/s1217709/reldem/")
london_namespace = Namespace("http://vocab.inf.ed.ac.uk/s1217709/london_area/")
nsMgr.bind('dbp', dbpedia_namespace)
nsMgr.bind('reldem', reldem_namespace)
nsMgr.bind('la', london_namespace)


with open("religion-ward-2001.csv", "rb") as csv_file:
    reader = csv.DictReader(csv_file)
    for row in reader:
        if row["Area name"] == "England and Wales":
            continue
        if row["Area name"] == "St Katherine`s and Wapping":
            row["Area name"] = "St. Katharine's and Wapping"
        rurl = "http://data.ordnancesurvey.co.uk/datasets/os-linked-data/apis/search?query=label:\"" + row["Area name"].replace('`', "'") + "\""
        r = requests.get(rurl)

        loc = URIRef(r.json()["results"][0]["link"])
        uriref = london_namespace[row["Area code"]]
        g.add( (uriref, RDF.about, loc))
        g.add( (uriref, dbpedia_namespace.officialName, Literal(row["Area name"])) )
        g.add( (uriref, dbpedia_namespace.londonBorough, Literal(row["Borough"])) )
        g.add( (uriref, dbpedia_namespace.populationTotal, Literal(row["All people"])))
        for religion in ["Christian", "Buddhist", "Hindu", "Jewish", "Muslim", "Sikh", "Other religions", "No religion", "Religion not stated"]:
            ind = urllib.quote_plus(str(row["Area code"] + religion))
            g.add( (reldem_namespace[ind], RDF.about, uriref) )
            g.add( (reldem_namespace[ind], dbpedia_namespace.religion, dbpedia_resources[religion.replace(" ", "_")]))
            g.add( (reldem_namespace[ind], reldem_namespace.percentageInArea, Literal(row["% " + religion])))
            g.add( (reldem_namespace[ind], reldem_namespace.numberInArea, Literal(row[religion])))


print g.serialize("output.ttl", format="turtle")
